FROM golang:1.10-alpine as builder

RUN apk add --no-cache git make gcc musl-dev linux-headers

RUN go get -v github.com/getamis/istanbul-tools/cmd/istanbul

RUN git clone https://github.com/jpmorganchase/quorum.git /tmp/quorum && \
	cd /tmp/quorum && \
	make all

ADD server /tmp/server
RUN cd /tmp/server && \
	go build -o gethserver -i *.go

FROM alpine:latest

RUN apk --update add --no-cache bash jq curl
COPY --from=builder /tmp/quorum/build/bin/* /usr/local/bin/
COPY --from=builder /go/bin/istanbul /usr/local/bin/
COPY --from=builder /tmp/server/gethserver /usr/local/bin/

ENV MASTER_NODE 1
ENV MASTER_IP 127.0.0.1
ENV MASTER_PORT 8080
ENV TARGETGASLIMIT "10000000"
ENV GETH_NODE_PORT 20000
ENV RPC_ADDR "0.0.0.0"
ENV RPC_PORT 20010
ENV WS_ADDR "0.0.0.0"
ENV WS_PORT 20020
ENV DATADIR /blockchain
ENV APIS "admin,db,eth,debug,miner,net,shh,txpool,personal,web3,quorum,istanbul"
ENV RPC_ARGS "--rpc --rpcaddr $RPC_ADDR --rpcport $RPC_PORT --rpcapi $APIS"
ENV WS_ARGS "--ws --wsaddr $WS_ADDR --wsport $WS_PORT --wsapi $APIS --wsorigins=*"
ENV PERMISSIONED "--permissioned"

ADD ./scripts /scripts

RUN chmod 0755 /scripts/*.sh

ENTRYPOINT exec /bin/bash -c "/scripts/run.sh"
