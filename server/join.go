package main

import (
	"net/http"
	"io/ioutil"
	"log"
	"strings"
	"errors"
)

func	getGenesisBlock(w http.ResponseWriter, req *http.Request) {
	datadir := Datadir

	if datadir == "" {
		datadir = "/blockchain"
	}

	http.ServeFile(w, req, datadir + "/quorum-genesis.json")
}

func	updateNodesJsonFiles(data []byte, body map[string]interface{}, datadir string) (error) {
	lastQuoteIndex := strings.LastIndex(string(data), "\"")
	if lastQuoteIndex == -1 {
		return errors.New("Failed to find \" in Nodes Json file data")
	}
	log.Println("Adding enode: " + body["enode"].(string) + " to network")
	newData := string(data)[:lastQuoteIndex + 1] + ",\n \"" + 
		body["enode"].(string) +"\"" + string(data)[lastQuoteIndex + 1:]
	err := ioutil.WriteFile(datadir + "/static-nodes.json", []byte(newData), 0644)
	err = ioutil.WriteFile(datadir + "/permissioned-nodes.json", []byte(newData), 0644)
	return err
}

func	joinNetwork(w http.ResponseWriter, req *http.Request) {
	datadir := Datadir

	if datadir == "" {
		datadir = "/blockchain"
	}

	body, err := extraJsonBodyFromRequest(req)
	if err != nil {
		log.Fatal("Error: ", err)
		return
	}
	if body == nil || body["enode"] == nil || body["enode"] == "" {
		log.Fatal("Enode is nil or empty")
		return
	}

	data, err := ioutil.ReadFile(datadir + "/static-nodes.json")
	if err != nil {
		log.Fatal("Error: ", err)
		return
	}

	err = updateNodesJsonFiles(data, body, datadir)
	if err != nil {
		log.Fatal("Error: ", err)
		return
	}
	http.ServeFile(w, req, datadir + "/static-nodes.json")
}
