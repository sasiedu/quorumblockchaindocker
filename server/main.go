package main

import (
	"net/http"
	"log"
)

func	indexRoute(res http.ResponseWriter, req *http.Request) {
	res.Write([]byte("Quorum index route"))
}

func	main() {

	http.HandleFunc("/", indexRoute)
	http.HandleFunc("/getGenesisBlock", getGenesisBlock)
	http.HandleFunc("/joinNetwork", joinNetwork)
	http.HandleFunc("/nodeJoinedNetwork", newNodeJoinedNetwork)

	log.Println("listen on: 8080")
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err)
		panic(err)
	}
}
