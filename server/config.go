package main

import (
	"os"
	"net/http"
	"io/ioutil"
	"encoding/json"
)

type Files struct {
	Data map[string]string
}

var IstanbulFiles = [2]string{"static-nodes.json", "quorum-genesis.json"}
var Datadir = os.Getenv("DATADIR")

func	extraJsonBodyFromRequest(req *http.Request) (map[string]interface{}, error) {
	var	body map[string]interface{}

	data, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return nil, err
	}
	if data == nil {
		return nil, nil
	}
	if err := json.Unmarshal(data, &body); err != nil {
		return nil, err
	}
	return body, nil
}
