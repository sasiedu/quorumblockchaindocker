package main

import (
	"log"
	"net/http"
	"os"
	"bufio"
	"strings"
	"io/ioutil"
)

func	isNodeInStaticList(enode string, datadir string) (bool, error) {
	file, err := os.Open(datadir + "/static-nodes.json")
	if err != nil {
		return false, err
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan()	{
		line := scanner.Text()
		if start := strings.Index(line, "\""); start > -1 {
			if end := strings.LastIndex(line, "\""); end > -1 {
				if line[start + 1:end] == enode {
					return true, nil
				}
			}
		}
	}
	if err = scanner.Err(); err != nil {
		return false, err
	}
	return false, nil
}

func	newNodeJoinedNetwork(w http.ResponseWriter, req *http.Request) {
	datadir := Datadir
	if datadir == "" {
		datadir = "/blockchain"
	}

	body, err := extraJsonBodyFromRequest(req)
	if err != nil {
		log.Fatal(err)
		return
	}
	if body == nil || body["enode"] == nil || body["enode"] == "" {
		log.Fatal("Node is empty or nil")
		return
	}

	enodeExist, err := isNodeInStaticList(body["enode"].(string), datadir)
	if err != nil {
		log.Fatal(err)
		return
	}
	if enodeExist == true {
		log.Println("Enode: ", body["enode"].(string), " already added to static-nodes")
		return
	}

	data, err := ioutil.ReadFile(datadir + "/static-nodes.json")
	if err != nil {
		log.Fatal(err)
		return
	}
	if err = updateNodesJsonFiles(data, body, datadir); err != nil {
		log.Fatal(err)
	} else {
		log.Println("Enode: ", body["enode"].(string), " added")
	}
}
