#!/bin/bash

FLAGS="--datadir $DATADIR --targetgaslimit $TARGETGASLIMIT --shh --port $GETH_NODE_PORT --unlock 0 --password $DATADIR/passwords.txt --syncmode full --mine --nodiscover $PERMISSIONED"

ALL_ARGS="$FLAGS $RPC_ARGS $WS_ARGS"

geth $ALL_ARGS
