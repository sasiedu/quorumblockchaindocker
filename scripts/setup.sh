#!/bin/bash

HOST_IP=$(hostname -i)
IP_ADDRESS=${PUBLIC_IP:-$HOST_IP}
echo "$IP_ADDRESS" > $DATADIR/ipaddress
GETH_NODE_PORT=${GETH_NODE_PORT:-20000}

CHAINID=${CHAINID:-4058}
BYZANTIUM_BLOCK=${BYZANTIUM_BLOCK:-1}

BALANCE=${BALANCE:-"0x446c3b15f9926687d2c40534fdb564000000000000"}

function checkIfDataDirAndFilesExist
{
	if [ ! -d $DATADIR ]; then
		mkdir -pv $DATADIR/geth
	fi

	if [ ! -f $DATADIR/output.log ]; then
		istanbul setup --nodes --verbose --num 1 --quorum >> $DATADIR/output.log
	fi

	if [ ! -f $DATADIR/account ]; then
		# { echo ''; echo '' } is add for when geth account new requests for a passphrase
		{ echo ''; echo''; } | geth --datadir $DATADIR account new | \
		tail -1 | sed s/Address\:\ \{//g | \
			sed s/\}//g | tee > $DATADIR/account
	fi

	if [ ! -f $DATADIR/nodeaddress ]; then
		sed -n '3p' < $DATADIR/output.log | sed s/\"Address\":\ \"//g | \
			sed s/\"//g | sed s/\,//g | sed s/\\t//g | tee > $DATADIR/nodeaddress
	fi

	if [ ! -f $DATADIR/nodeinfo ]; then
		sed -n '5p' < $DATADIR/output.log | sed s/\"NodeInfo\":\ //g | \
			sed s/${ENODE_REPLACE:-0.0.0.0:30303?discport=0}/$IP_ADDRESS:$GETH_NODE_PORT/g | \
			sed s/\\t//g | tee > $DATADIR/nodeinfo
	fi

	if [ ! -f $DATADIR/geth/nodekey ]; then
		sed -n '4p' < $DATADIR/output.log | sed s/\"Nodekey\":\ \"//g | \
			sed s/\"//g | sed s/\,//g | sed s/\\t//g | tee > $DATADIR/geth/nodekey
	fi

	if [ ! -f $DATADIR/passwords.txt ]; then
		touch $DATADIR/passwords.txt
	fi
}

function loadDataFromFiles
{
	ACCOUNT=$(cat $DATADIR/account)
	NODE_ADDRESS=$(cat $DATADIR/nodeaddress)
	NODE_INFO=$(cat $DATADIR/nodeinfo)
	NODE_KEY=$(cat $DATADIR/geth/nodekey)
	echo "ACCOUNT: $ACCOUNT"
	echo "NODE_ADDRESS: $NODE_ADDRESS"
	echo "NODE_INFO: $NODE_INFO"
	echo "NODE_KEY: $NODE_KEY"
}

function isMasterNode
{
	if [ ! -f $DATADIR/quorum-genesis.json ]; then
		sed -n '18,200p' < $DATADIR/output.log | \
			jq --argjson id $CHAINID '.config.chainId = $id' | \
			jq --argjson entry "{\"byzantiumBlock\": $BYZANTIUM_BLOCK}" '.config += $entry' | \
			jq --argjson entry "{\"$ACCOUNT\": {\"balance\": \"$BALANCE\"}}" '.alloc += $entry' \
		| tee > $DATADIR/quorum-genesis.json
	fi

	if [ ! -f $DATADIR/static-nodes.json ]; then
		echo -e "[\n $NODE_INFO\n]" > $DATADIR/static-nodes.json
	fi
	
	if [ ! -f $DATADIR/permissioned-nodes.json ]; then
		echo -e "[\n $NODE_INFO\n]" > $DATADIR/permissioned-nodes.json
	fi
}

function	joinNetwork
{
	NODE_INFO=$(cat $DATADIR/nodeinfo | sed s/\"//g)
	curl -X GET http://$MASTER_IP:$MASTER_PORT/getGenesisBlock > $DATADIR/quorum-genesis.json
	curl -X POST -H "Content-Type: application/json" -d '{"enode": "'"$NODE_INFO"'"}' \
		http://$MASTER_IP:$MASTER_PORT/joinNetwork > $DATADIR/static-nodes.json
	
	block=$(cat $DATADIR/quorum-genesis.json)
	if [[ ${block:0:1} != "{" ]]; then
		echo "Failed to get genesis block"
		cat $DATADIR/quorum-genesis.json
		sleep 5
		echo "Trying to connect to master node again....."
		joinNetwork
	fi

	nodes=$(cat $DATADIR/static-nodes.json)
	if [[ ${nodes:0:1} != "[" ]]; then
		echo "Failed to get static-nodes"
		cat $DATADIR/static-nodes.json
		sleep 5
		echo "Trying to connect to master node again....."
		joinNetwork
	fi
	
	cp $DATADIR/static-nodes.json $DATADIR/permissioned-nodes.json
}

checkIfDataDirAndFilesExist && loadDataFromFiles

if [ "$MASTER_NODE" == 1 ]; then
	isMasterNode
else
	joinNetwork
fi
geth --datadir $DATADIR init $DATADIR/quorum-genesis.json
